#include "../include/bmp_format.h"

#include "../include/image_utils.h"

#define FILE_HEADER_BITS 0x4d42
#define BITS_PER_PIXEL 24

// Structure representing the BMP header
struct __attribute__((__packed__)) bmp_header {
	uint16_t bfType;             // File type signature
	uint32_t bfileSize;          // File size
	uint32_t bfReserved;         // Reserved, should be 0
	uint32_t bOffBits;           // Offset to pixel data
	uint32_t biSize;             // Size of the BMP header
	uint32_t biWidth;            // Image width
	uint32_t biHeight;           // Image height
	uint16_t biPlanes;           // Number of color planes
	uint16_t biBitCount;         // Bits per pixel
	uint32_t biCompression;      // Compression method
	uint32_t biSizeImage;        // Size of the raw image data
	uint32_t biXPelsPerMeter;    // Horizontal resolution (pixels per meter)
	uint32_t biYPelsPerMeter;    // Vertical resolution (pixels per meter)
	uint32_t biClrUsed;          // Number of colors in the color palette
	uint32_t biClrImportant;     // Number of important colors
};

// Function to create a new BMP header with the specified width and height
struct bmp_header new_bmp_header(size_t width, size_t height) {
	struct bmp_header header;
	header.bfType = FILE_HEADER_BITS;                                                // File type signature
	header.bfileSize = sizeof(struct bmp_header) + 3 * width * height + (width * BITS_PER_PIXEL / 8) % 4 * height; // File size
	header.bfReserved = 0;                                                          // Reserved, should be 0
	header.bOffBits = sizeof(struct bmp_header);                                     // Offset to pixel data
	header.biSize = sizeof(struct bmp_header) - sizeof(uint32_t) * 3 - sizeof(uint16_t); // Size of the BMP header
	header.biWidth = width;                                                          // Image width
	header.biHeight = height;                                                        // Image height
	header.biPlanes = 1;                                                             // Number of color planes
	header.biBitCount = BITS_PER_PIXEL;                                              // Bits per pixel
	header.biCompression = 0;                                                        // Compression method
	header.biSizeImage = header.bfileSize - sizeof(struct bmp_header);                // Size of the raw image data
	header.biXPelsPerMeter = 2834;                                                   // Horizontal resolution (pixels per meter)
	header.biYPelsPerMeter = 2834;                                                   // Vertical resolution (pixels per meter)
	header.biClrUsed = 0;                                                            // Number of colors in the color palette
	header.biClrImportant = 0;                                                       // Number of important colors
	return header;
}


// Function to read an image from a file in BMP format
enum read_status file2bmp(FILE* in, struct image* img) {
	struct bmp_header header = {0};

	// Read the BMP header from the file
	if (fread(&header, sizeof(struct bmp_header), 1, in) > 0) {
		if (header.bfType != FILE_HEADER_BITS)
			return READ_BMP_INVALID_SIGNATURE;

		if (header.biBitCount != BITS_PER_PIXEL)
			return READ_BMP_INVALID_BITS;
	}

	// Skip any additional headers between the file header and pixel data
	if (fseek(in, (long)(header.bOffBits - sizeof(struct bmp_header)), SEEK_CUR))
		return READ_BMP_INVALID_HEADER;

	// Create an image with the specified width and height
	if (create_image(img, header.biWidth, header.biHeight))
		return READ_BMP_ALLOCATION_ERROR;

	const size_t line_size = image_line_bytes(header.biWidth);
	const uint8_t padding = (4 - line_size % 4) % 4;

	for (size_t i = 0; i < header.biHeight; ++i) {
		// Read pixel data for each line
		if (fread((void*)get_pixels(*img, i), line_size, 1, in) != 1) {
			free_image(img);
			return READ_BMP_INVALID_PIXELS;
		}

		// Skip any padding before the next line
		if (fseek(in, padding, SEEK_CUR)) {
			free_image(img);
			return READ_BMP_INVALID_PIXELS;
		}
	}

	return READ_BMP_OK;
}


// Function to write an image to a file in BMP format
enum write_status bmp2file(FILE* out, struct image const* img) {
	// Create a BMP header with the image width and height
	struct bmp_header header = new_bmp_header(get_width(*img), get_height(*img));

	// Write the BMP header to the file
	if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
		return WRITE_BMP_ERROR;

	const size_t line_size = image_line_bytes(header.biWidth);
	const uint8_t padding = (4 - line_size % 4) % 4;

	for (size_t i = 0; i < get_height(*img); ++i) {
		// Write pixel data for each line
		if (fwrite((void*)get_pixels(*img, i), line_size, 1, out) != 1)
			return WRITE_BMP_ERROR;

		// Write any padding after each line
		if (fseek(out, padding, SEEK_CUR))
			return WRITE_BMP_ERROR;
	}

	fwrite("\0", 1, 1, out);

	return WRITE_BMP_OK;
}


// Function to convert a read status enum value to an error message string
const char* read_status2str(enum read_status status) {
	const char* error_message = NULL;
	switch (status) {
		case READ_BMP_INVALID_SIGNATURE:
			error_message = "Invalid signature of file";
			break;
		case READ_BMP_INVALID_BITS:
			error_message = "Invalid bits per pixel, only 24 b/p is supported";
			break;
		case READ_BMP_INVALID_HEADER:
			error_message = "Invalid header";
			break;
		case READ_BMP_INVALID_PIXELS:
			error_message = "Invalid pixels array";
			break;
		case READ_BMP_ALLOCATION_ERROR:
			error_message = "Error allocating memory to load image";
			break;
		default:
			error_message = "Unknown error";
			break;
	}
	return error_message;
}

// Function to convert a write status enum value to an error message string
const char* write_status2str(enum write_status status){
	const char* error_message = NULL;
	switch (status) {
		case WRITE_BMP_ERROR:
			error_message = "Error write to file";
			break;
		default:
			error_message = "Unknown error";
			break;
	}
	return error_message;
}

