#include "../include/image_utils.h"

#include <stdlib.h>

// Structure representing a pixel with RGB color components
struct __attribute__((__packed__)) pixel {
	uint8_t b, g, r;
};

// Function to allocate memory for a new pixel array with given width and height
struct pixel* new_pixels(size_t width, size_t height);

// Function to create an image with the specified width and height
int create_image(struct image* img, uint64_t width, uint64_t height) {
	// Check if width and height are both zero
	if (!width && !height) {
		// Set image data to NULL and width/height to 0
		img->data = NULL;
		img->width = 0;
		img->height = 0;
		return 0;
	}
	
	// Allocate memory for the pixel array
	img->data = new_pixels(width, height);
	
	// Check if memory allocation was successful
	if (!img->data) {
		// Set width and height to -1 to indicate an error
		img->width = -1;
		img->height = -1;
		return 1;
	} else {
		// Set the width and height of the image
		img->width = width;
		img->height = height;
		return 0;
	}
}

// Function to free the memory allocated for an image
void free_image(struct image* img) {
	// Check if the image data is not NULL
	if (img->data) {
		free(img->data);
	}
	// Set image data to NULL and width/height to -1
	img->data = NULL;
	img->width = -1;
	img->height = -1;
}

// Function to allocate memory for a new pixel array with given width and height
struct pixel* new_pixels(size_t width, size_t height) {
	if (width * height * sizeof(struct pixel) <= 0)
		return NULL;
	return malloc(width * height * sizeof(struct pixel));
}

// Function to copy an image from the source image to the destination image
void copy_image(struct image const from, struct image* to) {
	// Check if the destination image pointer is NULL or the source image dimensions are invalid
	if (to == NULL || from.width < 0 || from.height < 0) {
		return;
	}
	
	// Check if the source image has zero width and height
	if (from.width == 0 || from.height == 0) {
		// Set destination image data to NULL and width/height to 0
		to->data = NULL;
		to->height = to->width = 0;
		return;
	}
	
	// Allocate memory for the pixel array of the destination image
	to->data = new_pixels(from.width, from.height);
	
	// Check if memory allocation was successful
	if (to->data == NULL) {
		// Set width and height of the destination image to -1 to indicate an error
		to->height = to->width = -1;
		return;
	}
	
	// Set the width and height of the destination image
	to->width = from.width;
	to->height = from.height;
	
	// Copy pixel data from the source to the destination image
	for (uint64_t y = 0; y < from.height; ++y) {
		for (uint64_t x = 0; x < from.width; ++x) {
			to->data[y * from.width + x] = from.data[y * from.width + x];
		}
	}
}

// Function to calculate the size of a row of pixels in bytes
size_t image_line_bytes(uint64_t width) {
	return width * sizeof(struct pixel);
}

// Function to get the width of an image
uint64_t get_width(struct image const img) {
	return img.width;
}

// Function to get the height of an image
uint64_t get_height(struct image const img) {
	return img.height;
}

// Function to get a pointer to the pixels of a specific line in an image
struct pixel* get_pixels(struct image const img, size_t line) {
	return img.data + (line * img.width);
}

// Function to get a pointer to a specific pixel in an image
struct pixel* get_pixel(struct image const img, uint64_t x, uint64_t y) {
	return img.data + (y * img.width + x);
}

// Function to set the value of a specific pixel in an image
void set_pixel(struct image img, uint64_t x, uint64_t y, struct pixel const* pix) {
	*(img.data + (y * img.width + x)) = *pix;
}

