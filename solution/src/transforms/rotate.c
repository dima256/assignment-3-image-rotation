#include "../../include/transforms/rotate.h"
#include "../../include/image_utils.h"

// Function to rotate an image by the specified angle
struct image rotate(struct image const img, int angle) {
	struct image rotated_img;

	const uint64_t width = get_width(img);
	const uint64_t height = get_height(img);

	// If the angle is a multiple of 180 degrees, the image size remains unchanged
	if (angle % 180)
		create_image(&rotated_img, height, width);
	else
		copy_image(img, &rotated_img);

	// If the creation of the rotated image fails, return an empty image
	if (rotated_img.height < 0 || rotated_img.width < 0)
		return rotated_img;

	switch (angle) {
		case 90:
			// Rotate the image 90 degrees
			for (uint64_t y = 0; y < width; ++y)
				for (uint64_t x = 0; x < height; ++x)
					set_pixel(rotated_img, x, width - y - 1, get_pixel(img, y, x));
			break;
		case 180:
			// Rotate the image 180 degrees
			for (uint64_t y = 0; y < height; ++y)
				for (uint64_t x = 0; x < width; ++x)
					set_pixel(rotated_img, width - x - 1, height - y - 1, get_pixel(img, x, y));
			break;
		case 270:
			// Rotate the image 270 degrees
			for (uint64_t y = 0; y < width; ++y)
				for (uint64_t x = 0; x < height; ++x)
					set_pixel(rotated_img, height - x - 1, y, get_pixel(img, y, x));
			break;
	}

	return rotated_img;
}

