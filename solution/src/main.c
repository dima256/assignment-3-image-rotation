#include "../include/bmp_format.h"
#include "../include/image_utils.h"
#include "../include/transforms/rotate.h"
#include "../include/utils.h"

int main(int argc, char** argv) {
	// Check if the correct number of command-line arguments is provided
	if (argc != 4) {
		print_error("Usage: %s <input> <output> <angle>\n", argv[0]);
		return 1;
	}

	// Extract the command-line arguments
	const char* input = argv[1];
	const char* output = argv[2];
	const int angle = parse_angle(argv[3]);

	// Check if the angle is valid
	if (angle < 0) {
		print_error("<angle> can only be '0', '90', '-90', '180', '-180', '270'");
		return 1;
	}

	FILE* input_file = NULL;
	// Open input file for reading
	const int open_file_read_err = open_file_r(input, &input_file);
	if (open_file_read_err || input_file == NULL) {
		log_file_error(input, open_file_read_err);
		return 1;
	}

	struct image img;
	// Read the input file and parse its contents into the 'img' structure
	const enum read_status r_status = file2bmp(input_file, &img);
	fclose(input_file);
	if (r_status) {
		print_error("%s", read_status2str(r_status));
		return 1;
	}

	struct image rotated_img = rotate(img, angle);
	free_image(&img);

	// Check if memory allocation for the rotated image was successful
	if (rotated_img.width < 0 || rotated_img.height < 0) {
		print_error("Error allocate memory to rotate image\n");
		return 1;
	}

	FILE* out_file = NULL;
	// Open output file for writing
	const int open_file_write_err = open_file_w(output, &out_file);
	if (open_file_write_err || !out_file) {
		log_file_error(input, open_file_write_err);
		free_image(&rotated_img);
		return 1;
	}

	// Write the rotated image to the output file
	const enum write_status w_status = bmp2file(out_file, &rotated_img);
	fclose(out_file);
	if (rotated_img.data) free_image(&rotated_img);
	if (w_status) {
		print_error("%s", write_status2str(w_status));
		return 1;
	}

	return 0;
}

