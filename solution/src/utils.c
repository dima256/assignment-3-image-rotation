#include "../include/utils.h"

#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

int open_file_r(const char* filename, FILE** file) {
	if (filename == NULL || file == NULL) {
		return -1; // Return error code to indicate invalid parameters
	}

	*file = fopen(filename, "rb");
	if (*file == NULL) {
		return errno; // Return error code to indicate file opening failure
	}

	return 0; // Return success code
}

int open_file_w(const char* filename, FILE** file) {
	if (filename == NULL || file == NULL) {
		return -1; // Return error code for invalid parameters
	}

	*file = fopen(filename, "wb");
	if (*file == NULL) {
		return errno; // Return error code for file opening failure
	}

	return 0; // Return success code
}

void log_file_error(const char* err, int errnum) {
	// Check if an error number is provided
	if(errnum>0) {
		// Print the file error message with the error number
		print_error("file %s error: %s\n", err, strerror(errnum));
	} else {
		// Print the file error message without an error number
		print_error("file %s error\n", err);
	}
}


int parse_angle(const char* str) {
    if (str == NULL) {
        return -1; // Return -1 for NULL input
    }

    if (strlen(str) > 0) { // Valid input string length check
        int angle = atoi(str); // Convert string to integer
        if (angle == 0 && str[0] != '0') {
            return -1; // Return -1 for invalid input (non-numeric)
        } else if (angle == 0 || angle == 90 || angle == 180 || angle == 270) {
            return angle; // Return the valid angle value
        } else if (angle == -90 || angle == -180 || angle == -270) {
            return angle+360; // Return the valid angle value
        }
    }

    return -1; // Return -1 for unrecognized input format
}

