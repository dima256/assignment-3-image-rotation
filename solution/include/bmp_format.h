#ifndef H367601_BMP_FORMAT
#define H367601_BMP_FORMAT

#include "image_utils.h"

#include <stdio.h>

enum read_status {
	READ_BMP_OK = 0,
	READ_BMP_INVALID_SIGNATURE,
	READ_BMP_INVALID_BITS,
	READ_BMP_INVALID_HEADER,
	READ_BMP_INVALID_PIXELS,
	READ_BMP_ALLOCATION_ERROR
};
enum write_status {
	WRITE_BMP_OK = 0,
	WRITE_BMP_ERROR
};

enum read_status file2bmp( FILE* in, struct image* img);
enum write_status bmp2file( FILE* out, struct image const* img);

const char* read_status2str(enum read_status status);
const char* write_status2str(enum write_status status);

#endif

