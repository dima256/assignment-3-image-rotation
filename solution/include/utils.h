#ifndef H367601_UTILS
#define H367601_UTILS

#include <stdint.h>
#include <stdio.h>

#define print_error(...) fprintf(stderr, __VA_ARGS__)

int open_file_r(const char* filename, FILE** file);
int open_file_w(const char* filename, FILE** file);

void log_file_error(const char* err, int errnum);
int parse_angle(const char* str);

#endif

