#ifndef H367601_IMAGE_UTILS
#define H367601_IMAGE_UTILS

#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>

struct pixel;

struct image {
	uint64_t width;
	uint64_t height;
	struct pixel* data;
};

int create_image(struct image* img, uint64_t width, uint64_t height);

void free_image(struct image* img);

void copy_image(struct image const from, struct image* to);

size_t image_line_bytes(uint64_t width);

uint64_t get_width(struct image const img);
uint64_t get_height(struct image const img);

struct pixel* get_pixels(struct image const img, size_t line);

struct pixel* get_pixel(struct image const img, uint64_t x, uint64_t y);

void set_pixel(struct image img, uint64_t x, uint64_t y, struct pixel const* pix);

#endif

