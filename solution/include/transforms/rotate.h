#ifndef H367601_ROTATE
#define H367601_ROTATE

#include "../image_utils.h"

struct image rotate(struct image const img, int angle);

#endif

